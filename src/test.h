//
// Created by Никита on 31.12.2021.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

//Прогнать все тесты
void run_tests();


//Обычное успешное выделение памяти.
void test_alloc(void* heap);

//Освобождение одного блока из нескольких выделенных.
void test_free_1(void* heap);

//Освобождение двух блоков из нескольких выделенных.
void test_free_2(void* heap);

//Память закончилась, новый регион памяти расширяет старый.
void test_grow_heap(void* heap);

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
void test_grow_heap_2(void* heap);


#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
