#ifndef _MEM_H_
#define _MEM_H_


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <sys/mman.h>

#define HEAP_START ((void*)0x04040000)

void* _malloc( size_t query );
void  _free( void* mem );
void* heap_init( size_t initial_size );

#define DEBUG_FIRST_BYTES 4

void debug_struct_info( FILE* f, void const* address );
void debug_heap( FILE* f,  void const* ptr );

//Эти функции понадобятся в test.c
size_t round_pages   ( size_t mem );
void* block_after( struct block_header const* block );
bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd );
struct block_header* block_get_header(void* contents);

#endif
