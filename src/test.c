//
// Created by Никита on 31.12.2021.
//
#define _DEFAULT_SOURCE

#include "test.h"
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24
#define HEAP_SIZE 10000


static void* _heap_init ();
static void _heap_free (void *heap);

void test_alloc(void* heap) {
    fprintf(stdout, "\n\n\n\nТЕСТ 1:\nОбычное успешное выделение памяти\n\n");

    struct block_header* first = heap;
    size_t heap_capacity_before = first->capacity.bytes;

    void* m1 = _malloc(0);
    void* m2 = _malloc(50);
    void* m3 = _malloc(100);
    void* m4 = _malloc(500);
    void* m5 = _malloc(1000);

    debug_heap(stdout, heap);


    if(!m1){
        err("\nТест 1: ПРОВАЛ:\nПервый блок не выделен\n");
    }

    if(!m2 || !m3 || !m4 || !m5){
        err("\nТест 1: ПРОВАЛ:\nПоследующие выделения блоков привели к ошибке\n");
    }

    const struct block_header* block_1 = block_get_header(m1);
    const struct block_header* block_2 = block_get_header(m2);
    const struct block_header* block_3 = block_get_header(m3);
    const struct block_header* block_4 = block_get_header(m4);
    const struct block_header* block_5 = block_get_header(m5);

    if(block_1->capacity.bytes != BLOCK_MIN_CAPACITY){
        err("\nТест 1: ПРОВАЛ:\nНекорректный выделенный объем минимального блока\n");
    } else{
        fprintf(stdout, "\nУспешное выделение блока с минимальным объемом\n");
    }

    if(block_2->capacity.bytes != 50 || block_3->capacity.bytes != 100
    || block_4->capacity.bytes != 500 || block_5->capacity.bytes != 1000){
        err("\nТест 1: ПРОВАЛ:\nВыделенный объем не совпадает с запросом\n");
    } else {
        fprintf(stdout, "\nУспешное выделение блоков с запрошенным объемом\n");
    }

    if(block_1->is_free || block_2->is_free || block_3->is_free
       || block_4->is_free  || block_5->is_free){
        err("\nТест 1: ПРОВАЛ:\nЗанятый блок помечен как свободный\n");
    } else {
        fprintf(stdout, "\nУспешная отметка всех блоков как занятых\n");
    }

    if(!blocks_continuous(block_1, block_2) || !blocks_continuous(block_2, block_3)
    || !blocks_continuous(block_3, block_4) || !blocks_continuous(block_4, block_5)){
        err("\nТест 1: ПРОВАЛ:\nБлоки выделены не вплотную\n");
    } else {
        fprintf(stdout, "\nУспешное выделение блоков вплотную\n");
    }


    _heap_free(heap);

    fprintf(stdout, "\nОчистка кучи\n\n");

    debug_heap(stdout, heap);

    size_t heap_capacity_after = first->capacity.bytes;

    if(heap_capacity_before != heap_capacity_after || !first->is_free){
        err("\nТест 1: ПРОВАЛ:\nНекорректная очистка кучи\n");
    } else {
        fprintf(stdout, "\nУспешная очистка кучи\n");
    }

    fprintf(stdout, "\nТЕСТ 1: УСПЕШНО\n\n\n\n");

}

void test_free_1(void* heap) {
    fprintf(stdout, "\n\nТЕСТ 2:\nОсвобождение одного блока из нескольких выделенных\n");
    _malloc(0);
    _malloc(50);
    void* m1 = _malloc(100);
    _malloc(500);
    _malloc(1000);

    fprintf(stdout, "\n\nКуча до освобождения\n");
    debug_heap(stdout, heap);

    _free(m1);

    fprintf(stdout, "\n\nКуча после освобождения\n");
    debug_heap(stdout, heap);

    const struct block_header* block = block_get_header(m1);

    if(!block->is_free){
        err("\nТест 2: ПРОВАЛ:\nОсвобожденный блок помечен как занятый\n");
    } else {
        fprintf(stdout, "\nУспешное освобождение блока\n");
    }

    _heap_free(heap);

    fprintf(stdout, "\nТЕСТ 2: УСПЕШНО\n\n\n\n");

}

void test_free_2(void* heap) {
    fprintf(stdout, "\n\nТЕСТ 3:\nОсвобождение двух блоков из нескольких выделенных.\n");
    _malloc(0);
    _malloc(50);
    void* m1 = _malloc(100);
    void* m2 = _malloc(500);
    _malloc(1000);

    const struct block_header* block_1 = block_get_header(m1);
    const struct block_header* block_2 = block_get_header(m2);

    size_t block_1_capacity = block_1->capacity.bytes;
    size_t block_2_capacity = block_2->capacity.bytes;

    fprintf(stdout, "\n\nКуча до освобождения\n");
    debug_heap(stdout, heap);

    _free(m2);
    _free(m1);

    fprintf(stdout, "\n\nКуча после освобождения\n");
    debug_heap(stdout, heap);


    if((!block_1->is_free)||(!block_2->is_free)){
        err("\nТест 3: ПРОВАЛ:\nОсвобожденный блок помечен как занятый\n");
    } else {
        fprintf(stdout, "\nУспешное освобождение блоков\n");
    }

    if(block_1->capacity.bytes != block_1_capacity + block_2_capacity + offsetof(struct block_header, contents)){
        err("\nТест 3: ПРОВАЛ:\nБлоки не объединились после освобождения\n");
    } else{
        fprintf(stdout, "\nУспешное объединение освобожденного блока со следующим\n");
    }

    _heap_free(heap);

    fprintf(stdout, "\nТЕСТ 3: УСПЕШНО\n\n\n\n");

}

void test_grow_heap(void* heap) {
    fprintf(stdout, "\n\nТЕСТ 4:\nПамять закончилась, новый регион памяти расширяет старый.\n");
    _malloc(0);
    _malloc(50);
    _malloc(100);
    _malloc(500);
    void* last = _malloc(1000);

    fprintf(stdout, "\n\nКуча до расширения\n");
    debug_heap(stdout, heap);

    void* new_region = _malloc(20000);

    const struct block_header* last_block = block_get_header(last);
    const struct block_header* new_block = block_get_header(new_region);


    fprintf(stdout, "\n\nКуча после расширения\n");
    debug_heap(stdout, heap);

    if(blocks_continuous(last_block, new_block)){
        fprintf(stdout, "\nУспешное расширение кучи вплотную\n");
    } else {
        err("\nТест 4: ПРОВАЛ:\nКуча выделилась не вплотную\n");
    }

    _heap_free(heap);

    fprintf(stdout, "\nТЕСТ 4: УСПЕШНО\n\n\n\n");

}


void test_grow_heap_2(void* heap) {
    fprintf(stdout, "\n\nТЕСТ 5:\nПамять закончилась, старый регион памяти не расширить из-за другого\n"
                    "выделенного диапазона адресов, новый регион выделяется в другом месте.\n");

    _malloc(0);
    _malloc(50);
    _malloc(100);
    _malloc(500);
    void* last = _malloc(1000);

    fprintf(stdout, "\n\n\nКуча до расширения\n\n");
    debug_heap(stdout, heap);

    const struct block_header* last_block = block_get_header(last);

    if(last_block->next){
        last_block = last_block->next;
    }

    //Создаем кучу, чтобы помешать расширить исходную вплотную
    const size_t RUBBISH_HEAP_SIZE = round_pages(4000);

    void* rubbish = mmap(block_after(last_block), RUBBISH_HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,0, 0);

    if (rubbish == MAP_FAILED) {
        err("\nТест 5: ПРОВАЛ:\nВспомогательная куча не выделилась корректно\n");
    } else {
        *((struct block_header *) rubbish) = (struct block_header) {
                .next = NULL,
                .capacity = capacity_from_size((block_size) {.bytes = RUBBISH_HEAP_SIZE}),
                .is_free = true
        };
        fprintf(stdout, "\n\n\nКуча, которая помешает выделению памяти вплотную\n\n");
        debug_heap(stdout, rubbish);
    }


    void* data = _malloc(50000);

    fprintf(stdout, "\n\n\nКуча после расширения\n\n");
    debug_heap(stdout, heap);

    const struct block_header* data_block = block_get_header(data);
    if (data_block == last_block) {
        err("\nТест 5: ПРОВАЛ:\nКуча выделилась вплотную\n");
    } else {
        fprintf(stdout, "\nУспешное расширение кучи не вплотную\n");
    }

    _heap_free(heap);

    fprintf(stdout, "\nТЕСТ 5: УСПЕШНО\n\n\n");
}


void run_tests(){
    void* heap = _heap_init(HEAP_SIZE);
    test_alloc(heap);
    test_free_1(heap);
    test_free_2(heap);
    test_grow_heap(heap);
    test_grow_heap_2(heap);
}

static void* _heap_init(size_t size){
    fprintf(stdout, "\n\n\nИнициализация кучи\n");
    void* heap = heap_init(size);
    debug_heap(stdout, heap);
    return heap;
}

static void _heap_free (void* heap){
    struct block_header* header = heap;
    while(header)
    {
        _free(header->contents);
        header = header->next;
    }
    _free(((struct block_header*) heap)->contents );
}