#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, const struct block_header * block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const* addr, size_t query ) {
    //получаем действительный размер
    query = region_actual_size(query);

    //Пробуем аллоцировать вплотную
    void* region_addr = map_pages(addr, query, MAP_FIXED_NOREPLACE);

    struct region region;

    //Не получилось аллоцировать вплотную - аллоцируем где получилось
    if (region_addr == MAP_FAILED) {
        region_addr = map_pages(addr, query, 0);
        region = (struct region)
                {.addr = region_addr, .size = query, .extends = false};
    } else {
        //А здесь получилось, блоки вплотную
        region = (struct region)
                {.addr = region_addr, .size = query, .extends = true};
    }

    //инициализируем первый блок в регионе
    block_init(region_addr, (block_size) {.bytes = query}, NULL);

    return region;
}



void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//После сохраняем исходному блоку указатель на новосозданный.
static bool split_if_too_big( struct block_header* block, size_t query ) {
    //Вдруг прилетел запрос меньше минимума
    query = size_max(query, BLOCK_MIN_CAPACITY);

    //Проверяем возможность разделения данного блока
    if(block && (block_splittable(block, query))){

        //Если разделение возможно, то считаем указатель на начало нового блока
        void* tail = (void*)((uint8_t*) block + offsetof(struct block_header, contents) + query);
        block_size tail_block_size = (block_size) {
            .bytes = block->capacity.bytes - query
        };

        //Создаем блок с посчитанной емкостью и указателем на следующий блок как у исходного.
        block_init(tail, tail_block_size, block->next);

        //Исходный же теперь нового объема и ссылается на остаток
        block->capacity.bytes = query;
        block->next = tail;

        return true;
    }

    return false;
}


/*  --- Слияние соседних свободных блоков --- */

void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

//Пытаемся объединить переданный блок со следующим, проверяем корректность текущего, следующего блока и возможность их объединения
//Если обхединение возможно, то увеличиваем емкость текущего блока на емкость объединенного и выставляем корректный указатель на следующий блок
//Объединяем в цикле до максимально возможного
static bool try_merge_with_next( struct block_header* block ) {
    bool is_merged = false;
    while( block && block->next && mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = block->next->next;
        is_merged = true;
    }
    return is_merged;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    //Если передан некорректный указатель ~ блок поврежден
    if(!block){
        return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
    }

    struct block_header* current = block;

    //Нужен чтобы передать указатель на последний блок, если ничего не найдем.
    struct block_header* last = block;

    while(current){
        //Если блок занят смысла рассматривать нет
        if(current->is_free) {
            //По максимуму объединяем блок, отщепим нужный кусок после.
            try_merge_with_next(current);
            //Если размер достаточен, то возвращаем "хороший" блок
            if (block_is_big_enough(sz, current)) {
                return (struct block_search_result)
                        {.type = BSR_FOUND_GOOD_BLOCK, .block = current};
            }
        }
        last = current;
        current = current->next;
    }
    //Ничего не нашли - передаем указатель на последний
    return (struct block_search_result)
            {.type = BSR_REACHED_END_NOT_FOUND, .block = last};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    //Перебираем кучу
    struct block_search_result block_result = find_good_or_last(block, query);
    //Отщипываем блок нужного размера если пришел хороший блок
    if(block_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(block_result.block, query);
    }
    return block_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    //Попытаемся выделить память вплотную
    void* region_addr = (void*)((uint8_t*) last + size_from_capacity(last->capacity).bytes);
    //Аллоцируем регион
    struct region new_region = alloc_region(region_addr, query);
    //Если не получилось выделить память
    if(region_is_invalid(&new_region)){
        return NULL;
    }
    //Блок уже был инициализирован по данному адресу
    struct block_header* block = new_region.addr;
    //Связываем
    last->next = block;

    //Проверяем начиная с последнего блока.
    //По возможности новосозданный блок объединиться с последним
    //В любом случае вернется блок нужного объема или произошла ошибка и вернем NULL
    struct block_search_result block_result = try_memalloc_existing(query, last);
    if(block_result.type == BSR_FOUND_GOOD_BLOCK){
        return block_result.block;
    }
    return NULL;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    //Проверяем, чтобы запрашиваемая емкость была не меньше минимальной
    query = size_max(query, BLOCK_MIN_CAPACITY);

    //Пытаемся найти блок в текущей куче
    struct block_search_result result = try_memalloc_existing(query, heap_start);

    switch(result.type){
        case BSR_REACHED_END_NOT_FOUND:
            return grow_heap(result.block, query);
        case BSR_FOUND_GOOD_BLOCK:
            return result.block;
        case BSR_CORRUPTED:
            return NULL;
    }
    return NULL;
}

void* _malloc( size_t query ) {
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr){
      //Помечаем как занятый и возвращаем, если пришел корректный адрес
      addr->is_free = false;
      return addr->contents;
  }
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  /*  Объединяем все последующие пустые блоки */
  try_merge_with_next(header);
}
